package example.com.ctapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void makeToast(View view){
        Toast toast  = new Toast(this);
        toast.setGravity(Gravity.CENTER,0,-300);
        toast.setDuration(Toast.LENGTH_LONG);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.customtoastlayout,(ViewGroup) findViewById(R.id.customLayout));
        toast.setView(v);
        toast.show();
    }
}
