package example.com.actvapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.MultiAutoCompleteTextView;

public class MainActivity extends AppCompatActivity {

    AutoCompleteTextView autoCompleteTextView;
    MultiAutoCompleteTextView multiAutoCompleteTextView;

    String[] dreams = {"---Select Any Dream---","Marry Me","Google Job","Luxary Cars","Ambani's Adopted Son"};
    String[] wish = {"---Select Any Wish---","I Could be God","Own a company","Travel for Life","Lots of gf/bf"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        multiAutoCompleteTextView = findViewById(R.id.multiAutoCompleteTextView);
        autoCompleteTextView = findViewById(R.id.autoCompleteTextView);

        ArrayAdapter dreamsAdapter  = new ArrayAdapter(this,android.R.layout.simple_expandable_list_item_1,dreams);
        ArrayAdapter wishAdapter  = new ArrayAdapter(this,android.R.layout.simple_expandable_list_item_1,wish);

        autoCompleteTextView.setThreshold(1);
        multiAutoCompleteTextView.setThreshold(1);

        multiAutoCompleteTextView.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        multiAutoCompleteTextView.setAdapter(wishAdapter);
        autoCompleteTextView.setAdapter(dreamsAdapter);

    }
}
