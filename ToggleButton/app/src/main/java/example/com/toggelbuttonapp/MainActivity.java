package example.com.toggelbuttonapp;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener{
    ToggleButton tButton;
    RelativeLayout rLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tButton = findViewById(R.id.toggleButton);
        tButton.setOnCheckedChangeListener(this);
        rLayout=findViewById(R.id.mainLayout);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView,boolean isChecked){
        if(isChecked){
            rLayout.setBackgroundColor(Color.CYAN);
            tButton.setBackgroundColor(Color.GREEN);
        }
        else{
            rLayout.setBackgroundColor(Color.RED);
            tButton.setBackgroundColor(Color.GRAY);
        }
    }

}
