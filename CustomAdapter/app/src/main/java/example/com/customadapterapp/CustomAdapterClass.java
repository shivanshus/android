package example.com.customadapterapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by shivanshu on 11/1/2017.
 */

public class CustomAdapterClass extends ArrayAdapter {
    Context context;
    String[] t;
    String[] d;
    int[] i;

    public CustomAdapterClass(@NonNull Context context,String[] title,String[] description,int[] images) {
        super(context,R.layout.customlayout,R.id.customLayout,description );
        this.context= context;
        this.t = title;
        this.d= description;
        this.i = images;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.customlayout,parent,false);

        TextView title = view.findViewById(R.id.title);
        TextView disc = view.findViewById(R.id.discription);
        ImageView imageView = view.findViewById(R.id.imageView);
        title.setText(t[position]);
        disc.setText(d[position]);
        imageView.setImageResource(i[position]);

        return view;
    }
}
