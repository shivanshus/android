package example.com.customadapterapp;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    String[] title;
    String[] description;
    int[] images = {R.drawable.a,R.drawable.b,R.drawable.c,R.drawable.d,R.drawable.e,R.drawable.f};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.lv);

        Resources resources = getResources();
        title = resources.getStringArray(R.array.title);
        description = resources.getStringArray(R.array.disc);

        CustomAdapterClass customObject = new CustomAdapterClass(this,title,description,images);
        listView.setAdapter(customObject);
    }
}
