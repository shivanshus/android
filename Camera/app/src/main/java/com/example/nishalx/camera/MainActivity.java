package com.example.nishalx.camera;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    ImageView imageView;
    int count;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = findViewById(R.id.imageView);

    }

    public void capture(View view) {
        Intent camera_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = getFile();
        camera_intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
        startActivityForResult(camera_intent,1);

    }

    private File getFile()
    {
        File folder = getExternalFilesDir("Camera App");

        if(!folder.exists())
        {
            folder.mkdir();
        }
        File image_file = new File(folder,"one"+count+".jpg");
        return image_file;
    }

    private int randomName(){
        Random rd  = new Random();
        int n= rd.nextInt(1000);
        count = n;
        return n;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String path = "/storage/emulated/0/Android/data/com.example.nishalx.camera/files/Camera App/one"+count
                +".jpg";
        imageView.setImageDrawable(Drawable.createFromPath(path));
    }
}
