package example.com.checkboxapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

public class BillingActivity extends AppCompatActivity {
    EditText et;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing);
        et = findViewById(R.id.bill);
        Intent intent = getIntent();
        String option = intent.getStringExtra("B");
        et.setText(option);
    }
}
