package example.com.checkboxapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    CheckBox indian,thai,chinees,italian;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        indian = findViewById(R.id.indian);
        thai = findViewById(R.id.thai);
        chinees = findViewById(R.id.chinees);
        italian = findViewById(R.id.italian);
    }

    public void order(View view){
        int totalAmount = 0;
        StringBuilder sb = new StringBuilder();
        sb.append("You Selected :- ");

        if(indian.isChecked()){
            sb.append("\nIndian:350/-");
            totalAmount +=350;
            //Intent i=new Intent(this,BillingActivity.class);
            Intent i=new Intent(getApplicationContext(),BillingActivity.class);
            i.putExtra("B",Integer.toString(totalAmount));
            startActivity(i);
        }
        if(thai.isChecked()){
            sb.append("\nThai:250/-");
            totalAmount +=250;
        }
        if(chinees.isChecked()){
            sb.append("\nChinees:150/-");
            totalAmount +=150;
        }
        if(italian.isChecked()){
            sb.append("\nItalian:50/-");
            totalAmount +=50;
        }

        sb.append("\nTotal Amount :"+totalAmount);
        Toast.makeText(this,sb.toString(), Toast.LENGTH_LONG).show();

    }
}
