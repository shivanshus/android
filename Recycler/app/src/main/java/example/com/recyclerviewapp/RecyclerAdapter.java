package example.com.recyclerviewapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by shivanshu on 11/3/2017.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder>{

    Context context;
    String[] title,descp;
    int[] img;

    RecyclerAdapter(Context context,String[] title,String[] description,int[] immage){
        this.context = context;
        this.title = title;
        this.descp = description;
        this.img = immage;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater  =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.displaylayout,parent,false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.title.setText(title[position]);
        holder.description.setText(descp[position]);
        holder.imageView.setImageResource(img[position]);
    }

    @Override
    public int getItemCount() {
        return img.length;
    }


    class RecyclerViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        TextView title,description;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            title=itemView.findViewById(R.id.textView);
            description=itemView.findViewById(R.id.textView2);
            imageView = itemView.findViewById(R.id.imageView);
        }
    }
}
