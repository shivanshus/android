package com.example.nishalx.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void frag1(View view) {
        android.support.v4.app.FragmentManager fragmentManager=getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction transaction=fragmentManager.beginTransaction();
        FrgmentOne one = new FrgmentOne();
        /*transaction.add(R.id.space,one);*/
        transaction.replace(R.id.space,one);
        transaction.addToBackStack("he");
        transaction.commit();

    }

    public void frag2(View view) {
        android.support.v4.app.FragmentManager fragmentManager=getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction transaction=fragmentManager.beginTransaction();
        FragmentTwo two = new FragmentTwo();
        /*transaction.add(R.id.space2,two);*/

        /*transaction.add(R.id.space,one);*/
        transaction.replace(R.id.space,two);
        transaction.addToBackStack("helo");
        transaction.commit();
    }

    public void frag3(View view) {
        android.support.v4.app.FragmentManager fragmentManager=getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction transaction=fragmentManager.beginTransaction();
        FragmentThree three = new FragmentThree();
        /*transaction.add(R.id.space3,one);*/
        transaction.replace(R.id.space,three);
        transaction.addToBackStack("h");
        transaction.commit();
    }
}
