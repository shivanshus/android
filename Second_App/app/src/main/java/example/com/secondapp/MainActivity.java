package example.com.secondapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("Shivanshu","onCreate is executed");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("Abhishek","onStart is executed");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Abhishek","onPause is executed");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("Abhishek","onResume is executed");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("Abhishek","onStop is executed");

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("Abhishek","onRestart is executed");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("Abhishek","onDestroy is executed");

    }
}
